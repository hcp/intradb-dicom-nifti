#!/bin/bash
set -x

HOST=${1}
PROJECT=${2}
SUBJECT=${3}
SESSION=${4}
SCAN=${5}
FILENAME=${6}
USER=${7}
PASS=${8}
BUILD_DIR=${9}
SCRIPT_DIR=${10}
REORIENT=${11}
SPLIT=${12}
DCM2NII_VERSION=${13}
JSESSIONID=${14}

OLD_IFS="$IFS"

echo  "Determine ALT_HOST to try to work around http vs https firewall issues.  Always try to use http when possible."
if [[ "$HOST" =~ ^https:.* ]] ; then
	ALT_HOST="$HOST"
	HOST=$(echo "$ALT_HOST" | sed -e "s/https:/http:/" -e "s/\/\/[^\/]*/&:8080/")	
elif [[ "$HOST" =~ ^http:.* ]] ; then
	ALT_HOST=$(echo "$HOST" | sed -e "s/http:/https:/" -e "s/:8080//")	
else
	ALT_HOST="$HOST"
fi
echo "HOST=${HOST}, ALT_HOST=${ALT_HOST}"

#dcm2niix_bin=/nrgpackages/tools.release/dcm2niix-2017.04.28/dcm2niix
dcm2niix_bin=/opt/app/intradb/dcm2niix/$DCM2NII_VERSION/dcm2niix
dcmdump_bin=/nrgpackages/tools/dcmtk/bin/dcmdump

# Set up build space
zip_dir=$BUILD_DIR/zips
dicom_dir=$BUILD_DIR/dicom/$SCAN/$FILENAME
nifti_dir=$BUILD_DIR/nifti/$SCAN
mkdir -p $zip_dir
mkdir -p $dicom_dir
mkdir -p $nifti_dir

# Set up FSL
export FSLDIR=/nrgpackages/tools.release/fsl-5.0.8-centos6_64
source /nrgpackages/tools.release/fsl-5.0.8-centos6_64/etc/fslconf/fsl.sh
export PATH=${FSLDIR}/bin:${PATH}

# Set up Python virtual environment
#source /data/intradb/home/hileman/python/venv/bin/activate
#singularity_container=/nrgpackages/tools.release/intradb/singularity/intradb-centos7-python3.sif

source /opt/app/intradb/bash-utilities/common_functions.sh

main () {
  # Set PCP status to RUNNING
  set_pcp_status
  # Get DICOM resource from XNAT
  download_dicom
  # Check if secondary scan and exit if so
  is_secondary_scan
  # Convert with dcm2niix depending on if setter series or not
  echo "Running dcm2niix v${DCM2NII_VERSION}..."
  if is_second_setter; then
    dcm2niix_setter
  else
    dcm2niix_standard
  fi
  # Reorient certain series descriptions
  reorient
  # Rename multi-echo files if appropriate
  rename_multi_echo
  # Split requested .nii.gz using fslroi
  split_nifti
  # Upload generated files to XNAT as NIFTI resource
  upload_nifti
}

set_pcp_status () {
  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"
  uri=$HOST/xapi/pipelineControlPanel/project/$PROJECT/pipeline/DCM2NII/entity/$SESSION/group/ALL/setValues?status=RUNNING
  curl -s -k --cookie "JSESSIONID=$JSESSIONID" $uri -X POST -H "Content-Type: application/json"
}

is_second_setter () {
  # Check if 2nd T1w_setter or T2w_setter series
  echo "Checking if this is second T1w or T2w setter."
  dicom_file=$(ls $dicom_dir | head -n 1)
  image_type=$($dcmdump_bin --search ImageType ${dicom_dir}/${dicom_file})
  sequence_name=$($dcmdump_bin --search SequenceName ${dicom_dir}/${dicom_file})

  if [[ $image_type == *"MOSAIC"* && $sequence_name == *"ABCD3d1_32ns"* ]]; then
    echo "2nd setter series found. Converting single frames then combining."
    return 0
  else
    return 1
  fi
}

download_dicom () {
  # Get the DICOM from scan in zip archive and extract
  echo "Getting DICOM resource zip"
  dicom_zip=${SESSION}_scan${SCAN}.zip
  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"
  scan_uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN
  uri=$scan_uri/resources/DICOM/files?format=zip
  echo "doCurl -f -s -k -v --cookie \"JSESSIONID=$JSESSIONID\" $uri -o $zip_dir/$dicom_zip"
  doCurl -f -s -k -v --cookie "JSESSIONID=$JSESSIONID" $uri -o $zip_dir/$dicom_zip
  ls -l $zip_dir/$dicom_zip
  unzip -o -j $zip_dir/$dicom_zip -d $dicom_dir
}

is_secondary_scan () {
  # Check dicom to see if this is a secondary scan and exit if so
  # source /nrgpackages/scripts/epd-python_setup.sh
  #singularity exec -B $dicom_dir $singularity_container python3 $SCRIPT_DIR/secondary-capture.py $dicom_dir
  python3 $SCRIPT_DIR/secondary-capture.py $dicom_dir
  if [ "$?" -eq "1" ]; then
    echo "$SCAN is a secondary capture"
    exit 0
  fi
}

dcm2niix_standard () {
  $dcm2niix_bin -z y -o $nifti_dir -b y -ba y -d n -f %f $dicom_dir
}

dcm2niix_setter () {
  # Convert each frame individually and combine with fslmerge
  #for dicom in $(find $dicom_dir -name "*dcm"); do
  # Some dicoms coming in without .dcm extension
  for dicom in $(find $dicom_dir -type f); do
    acq_num=$($dcmdump_bin --search AcquisitionNumber $dicom | \
      cut -d"[" -f 2 | cut -d"]" -f 1 | xargs -I '{}' printf "%03g" '{}')
    $dcm2niix_bin -z y -o $nifti_dir -b y -ba y -d n -s y -f %f_a${acq_num} $dicom
  done

  # Merge into single NIFTI file
  echo "fslmerge -t $nifti_dir/${FILENAME} $nifti_dir/${FILENAME}_a???.nii.gz"
  fslmerge -t $nifti_dir/${FILENAME} $nifti_dir/${FILENAME}_a???.nii.gz

  # Remove all the single frame nifti files
  rm -f $nifti_dir/${FILENAME}_a*.nii.gz

  # Combine all individual json files
  #singularity exec -B $nifti_dir $singularity_container python3 $SCRIPT_DIR/combine-json.py $nifti_dir $FILENAME
  python3 $SCRIPT_DIR/combine-json.py $nifti_dir $FILENAME

  # Remove all the single json files
  rm -f $nifti_dir/${FILENAME}_a*.json
}

rename_multi_echo () {
  # Check if multi-echo and add _e1 suffix to the first file
  if [[ $(ls ${nifti_dir}/*_e2.nii.gz) ]]; then
    echo "Renaming vNav files .."
    mv $nifti_dir/${FILENAME}.nii.gz $nifti_dir/${FILENAME}_e1.nii.gz
    mv $nifti_dir/${FILENAME}.json $nifti_dir/${FILENAME}_e1.json
  fi
}

reorient () {
  # See if it needs reoriented using fslreorient2std
  if [ $REORIENT = "true" ]; then
    echo "NOT adjusting the value for PhaseEncodingDirection in the original BIDS sidecar (.json) file."
    echo "Check whether PhaseEncodingDirection is still accurate after reorienting, and correct if necessary."

    for nifti in $(find $nifti_dir -name "*.nii.gz"); do
      echo "Reorienting $nifti"
      $SCRIPT_DIR/reorient.sh $nifti
    done
  fi
}

split_nifti () {
  if [[ $SPLIT -lt 1 ]]; then
    return
  fi

  mv $nifti_dir/$FILENAME.nii.gz $nifti_dir/${FILENAME}_orig.nii.gz
  fslroi $nifti_dir/${FILENAME}_orig.nii.gz $nifti_dir/${FILENAME}_InitialFrames.nii.gz 0 $SPLIT
  fslroi $nifti_dir/${FILENAME}_orig.nii.gz $nifti_dir/${FILENAME}.nii.gz $SPLIT -1

  if [ "$?" -eq "0" ]; then
    rm -f $nifti_dir/${FILENAME}_orig.nii.gz
  else
    exit 2
  fi
}

upload_nifti () {
  # Create NIFTI resource for scan and POST files to resource
  echo "Creating NIFTI resource and uploading files"
  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"
  scan_uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN
  uri=$scan_uri/resources/NIFTI
  curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X PUT $uri?format=NIFTI
  sleep 15
  echo "LIST NIFTI FILES ($nifti_dir):"
  find $nifti_dir -type f -ls 
  echo "SEND NIFTI FILES WITH &reference OPTION:"
  curl -f -s -k -v --cookie "JSESSIONID=$JSESSIONID" -X PUT $uri/files?overwrite=true\&replace\&reference=$nifti_dir 
}

echo "Calling refresh_jsession"
HOST_SESS=$(refresh_jsession)
IFS="," read -a HSARR <<< $HOST_SESS
JSESSIONID=${HSARR[0]}
HOST=${HSARR[1]}
IFS="$OLD_IFS"

echo "Calling main method"
main


