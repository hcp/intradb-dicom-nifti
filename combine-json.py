#!/usr/bin/env python
'''
Combines all json files from setter conversion 
Creates a list for any field with per-slice value
Returns 2 for json parse error
'''
import os
import sys
import json
import shutil
from collections import OrderedDict

try:
    nifti_dir = sys.argv[1]
    filename = sys.argv[2]
except IndexError as e:
    print("Need a nifti directory and filename as input")
    sys.exit(-1)

return_value = 0

# Get a list of all json files sorted by slice
json_files = []
for f in sorted(os.listdir(nifti_dir)):
    if f.endswith('.json'):
        # Use absolute paths
        json_files.append(os.path.join(nifti_dir, f))

# Collect all of the per-slice fields into lists
acquisition_number = []
acquisition_time = []
image_comments = []
image_orientation_patient = []

for f in json_files:
    with open(f) as json_data:
        try:
            json_str = json_data.read().replace('\r', ' ').replace('\n', ' ').replace('^M', ' ')
            obj = json.loads(json_str, strict=False)
        except ValueError as e:
            print(f)
            print(json_str)
            print(str(e))
            print("Could not parse JSON file " + f)
            shutil.copyfile(f, f+'.parse_error')
            return_value = 2
            continue

    acquisition_number.append(obj['AcquisitionNumber'])
    acquisition_time.append(obj['AcquisitionTime'])
    image_orientation_patient.append(obj['ImageOrientationPatientDICOM'])
    # Some slices don't have image comments
    try:
        image_comments.append(obj['ImageComments'])
    except KeyError as e:
        print(str(e))
        print("++ No image comment for " + f)
        print("++ Inserting blank image comment for slice")
        image_comments.append("")

# Put lists of fields back into single object and dump to json
with open(json_files[0]) as f:
    json_str = f.read().replace('\r', ' ').replace('\n', ' ').replace('^M', ' ')
    json_obj = json.loads(json_str, object_pairs_hook=OrderedDict)


json_obj['AcquisitionNumber'] = acquisition_number
json_obj['AcquisitionTime'] = acquisition_time
json_obj['ImageComments'] = image_comments
json_obj['ImageOrientationPatientDICOM'] = image_orientation_patient

json_out = os.path.join(nifti_dir, filename+'.json')
with open(json_out, 'w') as f:
    json.dump(json_obj, f, indent=4, sort_keys=False)

sys.exit(return_value)
