## Intradb Dicom to Nifti Pipeline

---
### Launcher -- convert-session.py
Parent script that handles authentication and running/submitting the dicom-to-nifti conversion job for each scan to Sun Grid Engine or as a local process.

##### Input parameters:
* hostname
* project
* subject 
*--Optional, will determine from project/session if possible*
* session
* scans 
*--Optional comma separated list of scans. If unused or "all", convert all scans.*
* submission-type (One of multiproc or sge. Default multiproc)
* build-root
* log-root
* existing 
*--For existing NIFTI resources, either skip, overwrite, or fail. Default skip.*

##### Features:
* Gets session Json and decides which scans to convert.
* Deletes existing NIFTI for all if --existing=overwirte option.
* Gets auth token for use by per scan script.
* Creates build space path and passes along for input and output.
* Manages parent process logging and combines scan logs.
* Submits job per scan using one of the following methods:
    - Multi-proc - uses multiple processes on the same machine 
    - SGE - uses qsub to submit to grid engine
    - TBD Docker - uses XNAT container service to submit to Docker container

---
### Subprocess -- dcm2nii-scan.sh

For one scan, pulls down DICOM resource, unzips, converts to NIFTI,
and POSTs back as a scan resource.
##### Input parameters:
* JSESSIONID
* HOST
* PROJECT
* SUBJECT
* SESSION
* SCAN
* BUILD_DIR
* SCRIPT_DIR

---
#### xnat-automation.groovy
Add the contents of this as an XNAT automation script.
