import json
import os
import sys
import shutil
import threading

sys.path.append("/nrgpackages/tools.release/intradb")

from IntradbPipeline.params import parser
from IntradbPipeline.common import Pipeline
from hcpxnat.interface import HcpInterface


# Add params specific to this pipeline here
# Other parameters are pulled from the generic params.py
parser.add_option("-r", "--reorient-series", action="store", type="string",
                  dest="reorient_series", default="none", help='List of ' +
                  'series descriptions to run fslreorient2std. Default none.')
parser.add_option("-f", "--split-at-frame", action="store", type="string",
                  dest="split_frame", default=None, help='Split the nii at ' +
                  'the given frame for a particular series using fslroi.\n' +
                  'e.g., tfMRI:10,rfMRI:15')
parser.add_option("-k", "--skip-split-by-series-descriptions", action="store", type="string",
                  dest="skip_split_for_series_descriptions", default=None, help='skip split for ' +
                  'the given scan tasks on the basis of series descriptions.\n' +
                  'e.g., tfMRI_FACEMACTHING_AP,tfMRI_CARIT_PA')				  
(opts, args) = parser.parse_args()

xnat = HcpInterface(
    url=opts.hostname, username=opts.alias, password=opts.secret)
xnat.project = opts.project
xnat.session_label = opts.session
# Take an optional subject label, otherwise find the subject label
xnat.subject_label = xnat.getSessionSubject()
version_config = json.load(open("/opt/app/intradb/dcm2niix/project-version-config.json"))

# Setup Pipeline object to use common pipeline functionality
pipe = Pipeline(xnat, opts, name='d2n')


def main():
    scans = getScans()

    if opts.existing == 'overwrite':
        pipe.deleteExistingResources('NIFTI', scans)

    runDicomToNifti(scans)
    pipe.combineScanLogs()
#    shutil.rmtree(pipe.build_dir)

def getConfiguredVersion(project):
	project_specific_versions = version_config['PROJECT_SPECIFIC_VERSIONS']
	for version in project_specific_versions:
		if project in project_specific_versions[version]:
			return version 
	return version_config['DEFAULT_VERSION']

def getScans():
    scan_ids = set()
    skip_ids = set()

    # Process only user provided scans if any
    scans_param = opts.scans.split(',') if opts.scans else []
    # Allow 'all' for param for handling by XNAT automation service
    if 'all' in scans_param:
        scans_param.remove('all')

    for scan in xnat.getSessionScans():
        xnat.scan_id = scan['ID']

        # Handle user supplied scan list
        if scans_param and xnat.scan_id not in scans_param:
            continue

        resources = xnat.getScanResources()

        for r in resources:
            # Find everything with a DICOM resource
            if r['label'] == 'DICOM':
                scan_ids.add(xnat.scan_id)

            # Check for existing NIFTI and exit if existing set to 'fail'
            elif r['label'] == 'NIFTI' and opts.existing == 'fail':
                pipe.log.warn(
                    "Aborting Dicom to Nifti. Set to fail on existing NIFTI")
                pipe.log.warn(
                    "Scan {} already has a NIFTI resource".format(scan['ID']))
                sys.exit(1)

            # Skip scans with NIFTI if existing set to 'skip'
            elif r['label'] == 'NIFTI' and opts.existing == 'skip':
                skip_ids.add(scan['ID'])

    if skip_ids:
        pipe.log.info("Skipping scans with existing NIFTI {}".format(
            ','.join(skip_ids)))

    # Remove skipped and secondary scans and return as a list
    scan_list = list(scan_ids - skip_ids)
    pipe.log.info("Converting scans {} to NIFTI".format(','.join(scan_list)))
    return scan_list


def getScanType(scan_id):
    uri = "/data/projects/{p}/subjects/{s}/experiments/{e}/scans".format(
        p=opts.project, s=xnat.subject_label, e=opts.session)
    scans = xnat.getJson(uri)

    for scan in scans:
        if scan['ID'] == scan_id:
            return scan['type']

    return None


def runDicomToNifti(scans):
    # Create a job for each scan and submit a process locally or to sge
    pwd = os.path.dirname(os.path.realpath(__file__))
    script_path = os.path.join(pwd, 'dicom-nifti-scan.sh')
    singularity_cmd = "/nrgpackages/tools.release/intradb/singularity/run_singularity.sh"
    script_path = singularity_cmd + " " + script_path 
    jsessionid = xnat.get('/REST/JSESSIONID').text
    threads = []

    try:
        pipe.setPcpStatus('DCM2NII', "QUEUED")
    except:
        pipe.log.warn('Warning:  Could\'t set pipeline status to QUEUED (Possibly the entity doesn\'t yet exit)')

    for scan in scans:
        xnat.scan_id = scan
        this_series_desc = xnat.getScanXmlElement("xnat:series_description")
        this_scan_type = getScanType(scan)
        fname = "{}_{}".format(xnat.session_label, this_series_desc)

        # Is this a series description to reorient?
        reorient = "false"
        if this_series_desc in opts.reorient_series.split(','):
            reorient = "true"

        dcm2nii_version  = getConfiguredVersion(opts.project)
        if hasattr('opts', 'dcm2nii_version'):
            dcm2nii_version = opts.dcm2nii_version

        # Should we split this scan type?
        split = 0
        if opts.split_frame:
            if ':' not in opts.split_frame:
                pipe.log.warn(
                    "--split-at-frame appears to be incorrectly specified")
            for type_frame in opts.split_frame.split(','):
                split_scan_type = type_frame.split(':')[0]
                split_frame = int(type_frame.split(':')[1])

                if this_scan_type == split_scan_type:
                    split = split_frame
					
			# Exclude skipping intial frames on the basis of series description?
            # excludeSplit = "false"
            if opts.skip_split_for_series_descriptions: #not opts.skip_split_for_series_descriptions:
                if this_series_desc in opts.skip_split_for_series_descriptions.split(','):
                    split = 0
                    pipe.log.info("Excluding "+this_series_desc+" form splitting.")

            if split > 0:
                pipe.log.info("Splitting scan {} at frame {}".format(
                    scan, split))

        cmd = "{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}".format(
            script_path,
            xnat.url,
            xnat.project,
            xnat.subject_label,
            xnat.session_label,
            scan,
            fname,
            xnat.username,
            xnat.password,
            pipe.build_dir,
            pwd,
            reorient,
            split,
            dcm2nii_version,
            jsessionid
        )

        pipe.log.info(cmd)

        t = threading.Thread(
            name=scan,
            target=pipe.submit,
            args=(scan, cmd, '', 'hcp_standard.q',)
        )
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

    try:
        pipe.setPcpEndStatus('DCM2NII')
    except:
        pipe.log.warn('Warning:  Could\'t set PCP end status (Possibly the entity doesn\'t yet exit)')

if __name__ == "__main__":
    main()

